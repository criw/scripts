# criw' scripts

## note
Each environment variable mentioned in this readme is
located in `.globals.template`, and scripts source that file.

Rename it to `.globals` and fill it before using anything from
here.

## required commands
* xclip --> required for `./c::pass_decrypt`

### c::pass_decrypt - kind of password manager
Passwords are located in `${PASSWORDS_HOME}` encrypted with GPG,
where each filename is the service (e.g: twitter). 

```
sample_file_decrypted
  user-someUsernameHere
  pass-somePasswordHere
```

Script grabs the file, decrypt it's content and copy it to 
the clipboard.

So if you want, let's say, your reddit account password:

```
./c::pass_decrypt reddit 
```

You can retrieve the user too if you have set it with 
`c::pass_encrypt`.

```
./c::pass_decrypt -u reddit   // copy user
./c::pass_decrypt -p reddit   // copy password
```

If you don't specify `-p` or `-u`, script will always give 
the password.

### c::pass_encrypt - this generates the encrypted files :D
This prompt you for information to generate the files used
in the script above and save them in `${PASSWORDS_HOME}`.

At this moment, it asks for:

```
service: (e.g. twitter)
username (blank if no needed):
password:
```

Furthemore, you need to specify `${SIGNATURE_MAIL}` which will
be the recipient GPG signs to.
